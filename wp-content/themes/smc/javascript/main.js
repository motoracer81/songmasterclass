jQuery(function($) {	
	
	$base_url = $('body').data('template');

	var y_scroll = 0;
		
	$(window).scroll(function(e){
		y_scroll = $(window).scrollTop();
		
		if( y_scroll > 50 ) 
		{
			$('.swim-menu').addClass('shadow');
		}
		else 
		{
			$('.swim-menu').removeClass('shadow');
		}	
	});
});

document.addEventListener("DOMContentLoaded", function () {

	var main = document.querySelector(".mobile-menu-wrapper"),
	    btnOpen = document.querySelector(".mobile-menu-btn"),
	    btnClose = document.querySelector(".mobile-menu-wrapper .close-btn");

	function mobileMenuToggle() {
	    main.classList.toggle("is-closed");
	    document.body.classList.toggle("no-scroll");
	}

	btnOpen.addEventListener("click", mobileMenuToggle);
	btnClose.addEventListener("click", mobileMenuToggle);

	var basicScrollTop = function () {  
	    var btnTop = document.querySelector('.smc-scroll-top');
	    var btnReveal = function () { 
	    if (window.scrollY >= 500) {
	      btnTop.classList.add('is-active');
	    } else {
	      btnTop.classList.remove('is-active');
	    }    
	  }  
	  var scrollTop = function () {
	    window.scrollTo({top: 0, behavior: 'smooth'});
	  }
	  window.addEventListener('scroll', btnReveal);
	  if(btnTop) btnTop.addEventListener('click', scrollTop);  
	    
	};
	basicScrollTop();
});