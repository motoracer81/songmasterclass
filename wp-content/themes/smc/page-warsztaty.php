<?php 
/* Template name: Warsztaty */
get_header(); 
?>
	<section class="content-wrapper">
		<div class="dashboard">
			<div class="container">
				<?php the_breadcrumb();?>	
			</div>
		</div>
		<div class="container">
			<h1 class="heading">
				<?php the_title();?>	
			</h1>
		</div>
		<?php 
			$data = get_posts(array(
				'post_type'		=> 'page',
				'posts_per_page'	=> -1,
				'suppress_filters' => 0,
				'orderby' => 'menu_order', 
    			'order' => 'DESC', 
				'post_parent' => icl_object_id(10, "page", true)
			));
			$a = 0;
			foreach($data as $row):
		?>
		<div class="workshop <?php if($a%2==0):?>light<?php endif;?>">
			<div class="container">
				<h2 class="lesson-heading"><?php echo $row->post_title;?></h2>
				<?php if(get_field('subheading', $row->ID)):?>
				<h5 class="lesson-subheading"><?php the_field('subheading', $row->ID);?></h5>
				<?php endif;?>
				<?php if(get_field('additional_button', $row->ID)):?>
				<a href="<?php the_field('additional_button_url', $row->ID);?>" class="buy-note" target="blank">
					<img src="<?php echo base_url;?>images/note.svg" alt="<?php the_field('additional_button', $row->ID);?>">
					<?php the_field('additional_button', $row->ID);?>
				</a>
				<?php endif;?>
				<?php 
					$song = get_posts(array(
						'post_type'		=> 'page',
						'posts_per_page'	=> -1,
						'suppress_filters' => 0,
						'orderby' => 'menu_order', 
		    			'order' => 'ASC', 
						'post_parent' => $row->ID
					));
					$i = 1;
					foreach($song as $key):
				?>
				<?php if($i == 1):?>
				<div class="lessons">
					<div class="lesson-box lessons-heading no-action"><?php the_field("lesson_tk", icl_object_id(10, "page", true));?></div>
					<div class="lesson-box lessons-heading no-action"><?php the_field("lesson_ln", icl_object_id(10, "page", true));?></div>
					<div class="lesson-box lessons-heading no-action"><?php the_field("lesson_both", icl_object_id(10, "page", true));?></div>
				</div>
				<?php endif;?>
				<h3>
					<?php echo $i;?>. <?php echo $key->post_title;?>
					<?php if(get_field('subheading', $key->ID)):?>
						- <span><?php the_field('subheading', $key->ID);?></span>
					<?php endif;?>
				</h3>
				<?php if(get_field('additional_button', $key->ID)):?>
				<a href="<?php the_field('additional_button_url', $key->ID);?>" class="buy-note" target="blank">
					<img src="<?php echo base_url;?>images/note.svg" alt="<?php the_field('additional_button', $key->ID);?>">
					<?php the_field('additional_button', $key->ID);?>
				</a>
				<?php endif;?>
				<?php 
					$tk = get_posts(array(
						'post_type'		=> 'lesson',
						'posts_per_page'	=> 1,
						'suppress_filters' => 0,
						'order' => 'ASC',
						'meta_query'		=> array(
							'relation' => 'AND',
							array(
								'key' => 'category',
								'value' => $key->ID,
								'compare' => '='
							),
							array(
								'key' => 'type',
								'value' => 'tk',
								'compare' => 'LIKE'
							)
						)
					));
					
					$ln = get_posts(array(
						'post_type'		=> 'lesson',
						'posts_per_page'	=> 1,
						'suppress_filters' => 0,
						'order' => 'ASC',
						'meta_query'		=> array(
							'relation' => 'AND',
							array(
								'key' => 'category',
								'value' => $key->ID,
								'compare' => '='
							),
							array(
								'key' => 'type',
								'value' => 'ln',
								'compare' => 'LIKE'
							)
						)
					));

					$both = get_posts(array(
						'post_type'		=> 'lesson',
						'posts_per_page'	=> 1,
						'suppress_filters' => 0,
						'order' => 'ASC',
						'meta_query'		=> array(
							'relation' => 'AND',
							array(
								'key' => 'category',
								'value' => $key->ID,
								'compare' => '='
							),
							array(
								'key' => 'type',
								'value' => 'both',
								'compare' => 'LIKE'
							)
						)
					));
				?>
				<div class="lessons">
					<?php if(!empty($tk)):?>
					<div class="lesson-box">
						<a href="<?php echo get_permalink($tk[0]->ID);?>">
							<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $tk[0]->ID);?>/0.jpg">
							<button class="play"></button>
						</a>
					</div>
					<?php endif;?>
					<?php if(!empty($ln)):?>
					<div class="lesson-box">
						<a href="<?php echo get_permalink($ln[0]->ID);?>">
							<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $ln[0]->ID);?>/0.jpg">
							<button class="play"></button>
						</a>
					</div>
					<?php endif;?>
					<?php if(!empty($both)):?>
					<div class="lesson-box">
						<a href="<?php echo get_permalink($both[0]->ID);?>">
							<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $both[0]->ID);?>/0.jpg">
							<button class="play"></button>
						</a>
					</div>
					<?php endif;?>
				</div>
			<?php $i++; endforeach;?>
			</div>
		</div>
		<?php $a++; endforeach;?>
	</section>
<?php wp_footer();?>
<?php get_footer();?>