<?php $lang = ICL_LANGUAGE_CODE;?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<title>
		<?php if( is_front_page()):?>
			<?php bloginfo( 'name' ); ?>
		<?php else:?>
			<?php wp_title(''); ?> - Song Master Class - Workshop - Konieczny & Napierała
		<?php endif;?>
	</title>
	<meta name="robots" content="index,follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="SHORTCUT ICON" href="<?php echo base_url;?>images/favicon.png" type="image/x-icon">
	<?php wp_head();?>	
</head>
<body <?php body_class();?>>
	<section class="swim-menu <?php if(is_page(icl_object_id(8, "page", true))):?>transparent<?php endif;?>">
		<header>
			<div class="container">
				<div class="smc-logo">
					<a href="<?php echo icl_get_home_url();?>" class="smc-logo-master">
						<img src="<?php the_field('logo', icl_object_id(8, "page", true));?>" alt="">
					</a>
					<img src="<?php the_field('logo_nck', icl_object_id(8, "page", true));?>" alt="" class="smc-logo-nck">
				</div>
				<div class="smc-nav">
					<div class="nav-strip">
						<form class="search-box" action="<?php echo home_url();?>">
							<input type="text" name="s" placeholder="<?php the_field('search', icl_object_id(8, 'page', true));?>">
							<input type="submit" value="<?php the_field('search_button', icl_object_id(8, 'page', true));?>"/>
						</form>
						<?php $data = get_field('social_media', icl_object_id(8, "page", true)); if(!empty($data)):?>
						<ul class="social-media">
							<?php foreach($data as $row):?>
							<li>
								<a href="<?php echo $row['link'];?>" target="blank">
									<img src="<?php echo $row['icon'];?>" alt="Social Media">
								</a>
							</li>	
							<?php endforeach;?>
						</ul>
						<?php endif;?>
						<div class="language">
							<?php language_switcher();?>
						</div>
					</div>
					<div class="mobile-menu-btn">
		                <img src="<?php echo base_url;?>images/mobile-menu.svg" alt="mobile-menu-btn">
		            </div>
		            <div class="mobile-menu-wrapper is-closed">
		                <div class="mobile-menu">
		                    <h4>Menu</h4>
		                    <div class="close-btn">
		                        <img src="<?php echo base_url;?>images/close.svg" alt="close-icon">
		                    </div>
		                    <ul>
		                    	<li <?php if($post->ID == icl_object_id(8, "page", true)):?>class="current-menu-item"<?php endif;?>>
									<a href="<?php echo icl_get_home_url();?>">
										<?php echo get_the_title(icl_object_id(8, "page", true));?>
									</a>
								</li>
		                        <li <?php if($post->ID == icl_object_id(10, "page", true)):?>class="current-menu-item"<?php endif;?>>
									<a href="<?php echo get_permalink(icl_object_id(10, "page", true));?>">
										<?php echo get_the_title(icl_object_id(10, "page", true));?>
									</a>
								</li>
								<li>
									<a data-fancybox href="<?php the_field("concert_video_url", icl_object_id(8, "page", true));?>">
										<?php the_field("menu_concert", icl_object_id(8, "page", true));?>
									</a>
								</li>
								<li <?php if($post->ID == icl_object_id(12, "page", true)):?>class="current-menu-item"<?php endif;?>>
									<a href="<?php echo get_permalink(icl_object_id(12, "page", true));?>">
										<?php echo get_the_title(icl_object_id(12, "page", true));?>
									</a>
								</li>
		                    </ul>
		                </div>
		            </div>
					<ul class="menu">
						<li <?php if($post->ID == icl_object_id(10, "page", true)):?>class="current-menu-item"<?php endif;?>>
							<a href="<?php echo get_permalink(icl_object_id(10, "page", true));?>">
								<?php echo get_the_title(icl_object_id(10, "page", true));?>
							</a>
						</li>
						<li>
							<a data-fancybox href="<?php the_field("concert_video_url", icl_object_id(8, "page", true));?>">
								<?php the_field("menu_concert", icl_object_id(8, "page", true));?>
							</a>
						</li>
						<li <?php if($post->ID == icl_object_id(12, "page", true)):?>class="current-menu-item"<?php endif;?>>
							<a href="<?php echo get_permalink(icl_object_id(12, "page", true));?>">
								<?php echo get_the_title(icl_object_id(12, "page", true));?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</header>
	</section>