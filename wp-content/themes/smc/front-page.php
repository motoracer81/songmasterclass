<?php 
/* Template name: Strona główna */
get_header(); 
?>
    <video class="video-js" autoplay loop muted playsinline>
    	<source src="" type="video/mp4">
  	</video>
  	<button class="video-sound"></button>
  	<script>
  	document.addEventListener("DOMContentLoaded", function() {
  		var video = document.querySelector('.video-js');
      var sources = video.getElementsByTagName('source');
  		var navSound = document.querySelector('.video-sound');

  		navSound.addEventListener("click", function(e){
  			e.currentTarget.classList.toggle("is-active");
  			video.muted = !video.muted;
  		});

      window.onresize = window.onload = function () {
          if (window.matchMedia("(orientation: portrait)").matches) {
            sources[0].src = '<?php the_field("video_mobile");?>';
            video.load();
          }

          if (window.matchMedia("(orientation: landscape)").matches) {
            sources[0].src = '<?php the_field("video_trailer");?>';
            video.load();
          }
      };
  	});
  	</script>
<?php wp_footer();?>
<?php get_footer();?>

