<?php

define( 'base_url', get_template_directory_uri().'/' );
	
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
}

register_nav_menus( array(
	'primary'   => __( 'Top primary menu', 'SongMasterClass' ),
	'secondary' => __( 'Secondary menu in left sidebar', 'SongMasterClass' ),
) );


add_action( 'admin_menu', 'remove_menu_pages' );
function remove_menu_pages() {
	remove_menu_page('link-manager.php'); //Odnośniki
	remove_menu_page('edit-comments.php'); // Komentarze	
	//remove_menu_page('plugins.php'); // Wtyczki
	remove_menu_page('users.php'); // Użytkownicy
	remove_menu_page('tools.php'); // Narzędzia
	remove_menu_page('edit.php'); // Wpisy
}

if ( function_exists( 'add_image_size' ) ) { 
	//add_image_size( 'gallery-box', 600, 540, true );
}

add_filter( "get_archives_link", "customarchives_link");
 
function customarchives_link( $x )
{
	$url = preg_match('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.-]*(\?\S+)?)?)?)@i', $x, $matches);
	
	return $matches[4] == $_SERVER['REQUEST_URI'] ? preg_replace('@<li@', '<li class="current-page-item"', $x) : $x;
}

if ( ! function_exists('single_repair'))
{
	function single_repair($data)
	{
		return preg_replace("/\s(\S)\s+/"," \\1&nbsp;", $data);
	}
}

// Load theme scripts (header.php)
function add_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
				
		wp_register_script('customscripts', get_template_directory_uri() . '/javascript/main.js', array('jquery'), '1.0.1', true); // Custom scripts
		wp_enqueue_script('customscripts'); // Enqueue it!

		wp_register_script('fancybox', get_template_directory_uri() . '/javascript/fancybox.min.js', array('jquery'), '1.0.1', true); // fancybox
		wp_enqueue_script('fancybox'); // Enqueue it!
    }
}

// Load template Blank styles
function template_styles()
{		
	wp_register_style('main', get_template_directory_uri() . '/css/main.css', array(), '1.0.0', 'all');
    wp_enqueue_style('main'); // Enqueue it!

    wp_register_style('fancybox', get_template_directory_uri() . '/css/fancybox.css', array(), '3.5.7', 'all');
    wp_enqueue_style('fancybox'); // Enqueue it!
}

// Add Actions
add_action('init', 'add_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'template_styles'); // Add Theme Stylesheet

function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function language_switcher(){
    $languages = icl_get_languages('skip_missing=1&orderby=code&order=asc');
    echo '<div class="yl-menu-switcher">
            <div class="languages ylms-'.ICL_LANGUAGE_CODE.'">';
    foreach($languages as $l){
        if(!$l['active']) {
            $langs[] = '<a href="'.$l['url'].'">'.$l['language_code'].'</a>';
        } else {
            $langs[] = '<div class="active">' . strtoupper($l['language_code']) .'</div>';
        }
    }
    echo join('', $langs);
    echo '</div></div>';
}

function the_breadcrumb() {
    global $post;
	$separator = "<span class='arrow'> | </span> "; 
	
    echo '<div class="breadcrumb">';
	if (!is_front_page()) {
		echo '<a href="';
		echo get_option('home');
		echo '">';
		_e('Home');
		echo "</a> ".$separator;
		if ( is_category() || is_single() ) {
			the_category(', ');
			if ( is_single() ) {
				echo $separator;
				echo '<span class="current-page">'.the_title().'</span>';
			}
		} elseif ( is_page() && $post->post_parent ) {
			$home = get_page(get_option('page_on_front'));
			for ($i = count($post->ancestors) - 1; $i >= 0; $i--) {
				if (($home->ID) != ($post->ancestors[$i])) {
					echo '<a href="';
					echo get_permalink($post->ancestors[$i]); 
					echo '">';
					echo get_the_title($post->ancestors[$i]);
					echo "</a>".$separator;
				}
			}
			$title = get_the_title();
			echo '<span class="current-page">'.$title.'</span>';
		} elseif (is_page()) {
			$title = get_the_title();
			echo '<span class="current-page">'.$title.'</span>';
		} elseif (is_404()) {
			echo "404";
		}
	} else {
		bloginfo('name');
	}
	echo '</div>';
}

remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');

function wpbeginner_remove_version() {
	return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

register_post_type( 'lesson', array(
	'labels'        =>  array( 'name' => __('Lekcje Video'), 'singular_name' => __('Lekcje Video'), 'add_new' => __( 'Dodaj video' ), ),
	'supports'      =>  array( 'title', 'editor', 'thumbnail', 'page-attributes', 'custom-fields' ),
	'rewrite' => array('slug' => 'warsztat', 'with_front' => true),
	'public'        =>  true,
	'has_archive'   =>  false,
	'exclude_from_search'   =>  true
));