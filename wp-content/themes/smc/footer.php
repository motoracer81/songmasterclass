	<?php get_template_part( 'templates/media', 'partners' );?>	
	<?php if(!is_page(icl_object_id(8, "page", true))):?>
	<footer>
		<div class="embassy">
			<div class="container">
				<?php echo single_repair(get_field("embassy", icl_object_id(8, "page", true)));?>
			</div>
		</div>
		<div class="container">
			<img src="<?php the_field('logo', icl_object_id(8, "page", true));?>" alt="">
			<p><?php the_field('copyright', icl_object_id(8, "page", true));?> <?php echo date('Y');?></p>
		</div>
	</footer>
	<button class="smc-scroll-top"></button>
	<?php endif;?>
</body>
</html>	