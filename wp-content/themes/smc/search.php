<?php
/**
Template Name: Search Page
*/
get_header(); 
?>
	<div id="contact-wrapper">
		<div class="dashboard">
			<div class="container">
				<div class="breadcrumb">
					<a href="<?php echo icl_get_home_url();?>"><?php echo get_the_title(icl_object_id(8, "page", true));?></a> <span class="arrow"> | </span>
					<?php the_field("search_results", icl_object_id(8, "page", true));?><span class="current-page"></span>
				</div>	
			</div>
		</div>
		<div class="container">
			<h1 class="heading">
				<?php the_field("search_results", icl_object_id(8, "page", true));?>
				<span class="phrase"> - <?php echo get_search_query();?></span>
			</h1>
			<?php $data = get_posts(array('post_type' => array( 'lesson' ), 'suppress_filters' => 0, 'posts_per_page' => -1, 's' => get_search_query()));?>
			<?php 
				if( !empty($data)):
			?>
			<ul class="search-results">
				<?php foreach($data as $row):?>
				<li>
					<a href="<?php echo get_permalink($row->ID);?>">
						<?php echo $row->post_title;?>
					</a>
				</li>
				<?php endforeach;?>
			</ul>
			<?php else:?>
			<div id="empty">
				<p><?php the_field('empty', icl_object_id(6, 'page', true));?></p>
				<form class="search-box" action="<?php echo home_url();?>">
					<input type="text" name="s" placeholder="<?php the_field('search', icl_object_id(8, 'page', true));?>">
					<input type="submit" value="<?php the_field('search_button', icl_object_id(8, 'page', true));?>"/>
				</form>
			</div>
			<?php endif;?>
		</div>
	</div>
<?php wp_footer();?>
<?php get_footer();?>

