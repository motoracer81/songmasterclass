<?php 
/* Template name: Artyści */
get_header(); 
?>
	<section class="content-wrapper">
		<div class="dashboard">
			<div class="container">
				<?php the_breadcrumb();?>	
			</div>
		</div>
		<div class="container">
			<div class="slider">	
			    <div class="slick-slider">
			        <?php $slider = get_field('slider'); foreach( $slider as $row ):?>
			        <img src="<?php echo $row['image'];?>" alt="">
			        <?php endforeach;?>
			    </div>
			</div>
			<div class="basic-text">
				<?php echo single_repair(get_field('content'));?>
			</div>
			<h1 class="heading">
				<?php the_title();?>	
			</h1>
			<div class="artists">
				<?php while(have_rows('artists')): the_row();?>
				<div class="person">
					<img src="<?php the_sub_field('image');?>" alt="<?php the_sub_field('heading');?>" class="aligncenter">
					<h2><?php the_sub_field('heading');?></h2>
					<div class="basic-text">
						<?php echo single_repair(get_sub_field('content'));?>
					</div>
					<div class="info">
						<div class="person-website-container">
							<a href="<?php the_sub_field('website_url');?>" class="person-website" target="blank">
								<?php the_sub_field('website_name');?>
							</a>
						</div>
						<?php $data = get_sub_field('social_media'); if(!empty($data)):?>
						<ul class="social-media">
							<?php foreach($data as $row):?>
							<li>
								<a href="<?php echo $row['link'];?>" target="blank">
									<img src="<?php echo $row['icon'];?>" alt="Social Media">
								</a>
							</li>	
							<?php endforeach;?>
						</ul>
						<?php endif;?>
					</div>
				</div>	
				<?php endwhile;?>	
			</div>
			<h2 class="heading"><?php the_field("heading");?></h2>
			<div class="person-website-container">
				<a href="http://<?php the_field('website_both');?>" class="person-website" target="blank">
					<?php the_field('website_both');?>
				</a>
				<a href="<?php the_field('website_fb_both');?>" target="blank" class="single-fb">
					<img src="<?php echo site_url();?>/wp-content/uploads/2020/08/facebook_bez.svg" alt="facebook">
				</a>
			</div>
		</div>
	</section>
	<script src="<?php echo base_url;?>javascript/slick.min.js"></script>
	<script>
	jQuery(function($){
	    $('.slick-slider').slick({
	        fade: true,
	        dots: true,
	        infinite: true,
	        speed: 1500,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        autoplay: true,
	        slickPause: 3000,
	        cssEase: 'linear'
	    });
	});
	</script>
<?php wp_footer();?>
<?php get_footer();?>