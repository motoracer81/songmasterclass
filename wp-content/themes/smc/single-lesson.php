<?php 
	get_header();
	$master_id = get_field('category_master');
	$id = get_field('category');
?>
	<section class="content-wrapper">
		<div class="dashboard">
			<div class="container">
				<div class="breadcrumb">
					<a href="<?php echo icl_get_home_url();?>"><?php echo get_the_title(icl_object_id(8, "page", true));?></a> <span class="arrow"> | </span>
					<a href="<?php echo get_permalink(icl_object_id(10, "page", true));?>"><?php echo get_the_title(icl_object_id(10, "page", true));?></a> <span class="arrow"> | </span>
					<?php the_title();?><span class="current-page"></span>
				</div>	
			</div>
		</div>
		<div class="container">
			<h2 class="lesson-heading"><?php echo get_the_title(icl_object_id($master_id, "page", true));?></h2>
			<?php if(get_field('subheading', icl_object_id($master_id, "page", true))):?>
			<h5 class="lesson-subheading"><?php the_field('subheading', icl_object_id($master_id, "page", true));?></h5>
			<?php endif;?>
			<?php if(get_field('additional_button', $master_id)):?>
			<a href="<?php the_field('additional_button_url', $master_id);?>" class="buy-note second" target="blank">
				<img src="<?php echo base_url;?>images/note.svg" alt="<?php the_field('additional_button', $master_id);?>">
				<?php the_field('additional_button', $master_id);?>
			</a>
			<?php endif;?>
			<h1 class="heading single-post">
				<?php the_title();?>
			</h1>
			<?php if(get_field('additional_button', $id)):?>
			<a href="<?php the_field('additional_button_url', $id);?>" class="buy-note" target="blank">
				<img src="<?php echo base_url;?>images/note.svg" alt="<?php the_field('additional_button', $id);?>">
				<?php the_field('additional_button', $id);?>
			</a>
			<?php endif;?>
			<?php 
				$single = get_posts(array(
					'post_type'		=> 'lesson',
					'posts_per_page'	=> -1,
					'suppress_filters' => 0,
					'order' => 'ASC',
					'meta_query'		=> array(
						'relation' => 'AND',
						array(
							'key' => 'category',
							'value' => $id,
							'compare' => '='
						)
					)
				));
			?>
			<div class="single-content">
				<div class="row">
					<div class="col-d-9 col-st-12">
						<iframe width="100%" height="550" src="https://www.youtube.com/embed/<?php the_field('video_youtube');?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
					<div class="col-d-3 col-st-12">
						<div class="lessons">
						<?php foreach($single as $val): if($post->ID != $val->ID):?>
							<?php $type = get_field('type', $val->ID);?>
							<div class="box-half">
								<div class="lessons-heading no-action"><?php the_field("lesson_$type", icl_object_id(10, "page", true));?></div>
								<div class="lesson-box single-post">
									<a href="<?php echo get_permalink($val->ID);?>">
										<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $val->ID);?>/0.jpg">
										<button class="play"></button>
									</a>
								</div>
							</div>
						<?php endif; endforeach;?>
						</div>
					</div>
				</div>
			</div>
			<h6><?php the_field("other_films", icl_object_id(10, "page", true));?></h6>
			<?php 
				$data = get_posts(array(
					'post_type'		=> 'page',
					'posts_per_page'	=> -1,
					'suppress_filters' => 0,
					'order' => 'ASC',
					'post_parent' => icl_object_id(10, "page", true)
				));
				$a = 0;
				foreach($data as $row):
					if($row->ID == $master_id):
			?>
			<div class="workshop light">
				<div>
					<?php 
						$song = get_posts(array(
							'post_type'		=> 'page',
							'posts_per_page'	=> -1,
							'suppress_filters' => 0,
							'order' => 'ASC',
							'post_parent' => $row->ID
						));
						$i = 1;
						foreach($song as $key):
							if($key->ID != $id):
					?>
					<?php if($i == 1):?>
					<div class="lessons">
						<div class="lesson-box lessons-heading no-action"><?php the_field("lesson_tk", icl_object_id(10, "page", true));?></div>
						<div class="lesson-box lessons-heading no-action"><?php the_field("lesson_ln", icl_object_id(10, "page", true));?></div>
						<div class="lesson-box lessons-heading no-action"><?php the_field("lesson_both", icl_object_id(10, "page", true));?></div>
					</div>
					<?php endif;?>
					<h3>
						<?php echo $i;?>. <?php echo $key->post_title;?>
						<?php if(get_field('subheading', $key->ID)):?>
							- <span><?php the_field('subheading', $key->ID);?></span>
						<?php endif;?>
					</h3>
					<?php if(get_field('additional_button', $key->ID)):?>
					<a href="<?php the_field('additional_button_url', $key->ID);?>" class="buy-note" target="blank">
						<img src="<?php echo base_url;?>images/note.svg" alt="<?php the_field('additional_button', $key->ID);?>">
						<?php the_field('additional_button', $key->ID);?>
					</a>
					<?php endif;?>
					<?php 
						$tk = get_posts(array(
							'post_type'		=> 'lesson',
							'posts_per_page'	=> 1,
							'suppress_filters' => 0,
							'order' => 'ASC',
							'meta_query'		=> array(
								'relation' => 'AND',
								array(
									'key' => 'category',
									'value' => $key->ID,
									'compare' => '='
								),
								array(
									'key' => 'type',
									'value' => 'tk',
									'compare' => 'LIKE'
								)
							)
						));
						
						$ln = get_posts(array(
							'post_type'		=> 'lesson',
							'posts_per_page'	=> 1,
							'suppress_filters' => 0,
							'order' => 'ASC',
							'meta_query'		=> array(
								'relation' => 'AND',
								array(
									'key' => 'category',
									'value' => $key->ID,
									'compare' => '='
								),
								array(
									'key' => 'type',
									'value' => 'ln',
									'compare' => 'LIKE'
								)
							)
						));

						$both = get_posts(array(
							'post_type'		=> 'lesson',
							'posts_per_page'	=> 1,
							'suppress_filters' => 0,
							'order' => 'ASC',
							'meta_query'		=> array(
								'relation' => 'AND',
								array(
									'key' => 'category',
									'value' => $key->ID,
									'compare' => '='
								),
								array(
									'key' => 'type',
									'value' => 'both',
									'compare' => 'LIKE'
								)
							)
						));
					?>
					<div class="lessons">
						<?php if(!empty($tk)):?>
						<div class="lesson-box">
							<a href="<?php echo get_permalink($tk[0]->ID);?>">
								<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $tk[0]->ID);?>/0.jpg">
								<button class="play"></button>
							</a>
						</div>
						<?php endif;?>
						<?php if(!empty($ln)):?>
						<div class="lesson-box">
							<a href="<?php echo get_permalink($ln[0]->ID);?>">
								<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $ln[0]->ID);?>/0.jpg">
								<button class="play"></button>
							</a>
						</div>
						<?php endif;?>
						<?php if(!empty($both)):?>
						<div class="lesson-box">
							<a href="<?php echo get_permalink($both[0]->ID);?>">
								<img src="https://img.youtube.com/vi/<?php the_field('video_youtube', $both[0]->ID);?>/0.jpg">
								<button class="play"></button>
							</a>
						</div>
						<?php endif;?>
					</div>
				<?php $i++; endif; endforeach;?>
				</div>
			</div>
			<?php $a++; endif; endforeach;?>
			<div class="btn-container">
				<a href="<?php echo get_permalink(icl_object_id(10, "page", true));?>" class="btn">
					<?php the_field("back_button", icl_object_id(10, "page", true));?>
				</a>
			</div>
		</div>
	</section>
	<script>
	jQuery(function($) {	
		$('ul.menu>li:first').addClass('current-menu-item');
	});
	</script>
<?php wp_footer();?>
<?php get_footer();?>

		