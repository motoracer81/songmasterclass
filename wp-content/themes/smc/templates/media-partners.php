<section class="media-partners <?php if(is_page(icl_object_id(8, "page", true))):?>bottom-site<?php endif;?>">
	<div class="container">
		<img src="<?php echo base_url;?>images/logo-black-sygnet.svg" alt="" class="aligncenter sygnet">
		<?php if(!is_page(icl_object_id(8, "page", true))):?>
		<h3 class="heading smaller"><?php the_field("media_partners_heading", icl_object_id(8, "page", true));?></h3>
		<?php endif;?>
		<ul>
			<?php while(have_rows('media_partners', icl_object_id(8, "page", true))): the_row();?>
			<li>
				<img src="<?php the_sub_field('logo');?>" alt="">
			</li>
			<?php endwhile;?>	
		</ul>
	</div>
</section>